//  1
// let firstName = 'Dmytro'
// let lastName = 'Kytsun'
// let age = 21

// alert(firstName)
// console.log(firstName)

// alert(lastName)
// console.log(lastName)

// alert(age)
// console.log(age)


//  2
// const FIRSTNAME = 'Dmytro'
// const LASTNAME = 'Kytsun'
// const AGE = 21

// alert(FIRSTNAME)
// console.log(FIRSTNAME)

// alert(LASTNAME)
// console.log(LASTNAME)

// alert(AGE)
// console.log(AGE)


//  3
// let lastName = prompt('Ваша фамилия?')
// let firstName = prompt('Ваше имя?')
// let age = prompt('Сколько вам лет?' )

// alert(`${lastName}
// ${firstName}
// ${age}`)

// console.log(lastName)
// console.log(firstName)
// console.log(age)


//  4
// let question = confirm('Есть ли вам 18 лет?')
// if (question) {
//         alert('Проходите')
//         console.log('Проходите')
//     } else {
//         alert('Подождите когда исполнится 18!')
//         console.log('Подождите когда исполнится 18!')
//     }
// console.log(question)


//  5
// let newDate = Date()
// alert(newDate)
// console.log(newDate)


//  6
// let newDate = Date()
// prompt(newDate)
// // alert(newDate)
// // console.log(newDate)


//  7
// let a = prompt('Введите число')
// let b = prompt('Введите число')
// let c = Number(a)+Number(b);
// alert(`Ваш результат = ${c}`)


//  8
// const GREY = '#ccc'
// const CRIMSON = '#DC143C'
// const WHEAT = '#F5DEB3'
// document.body.style.background = WHEAT	


//  9
// let massColor = ['#F5DEB3', '#D2691E', '#6A5ACD', '#ADD8E6', '#FFFACD',]
// document.body.style.background = massColor[3]	


//  10
// let objectColors = {
//     Azure: '#F0FFFF',
//     Gray: '#808080',
//     Tan: '#D2B48C',
//     DarkSlateBlue: '#483D8B',
//     Lavender: '#E6E6FA'
// }
// document.body.style.background = objectColors.Azure


//  11
// let person = {
//     firstName: 'Stella',
//     lastName: 'Adderiy',
//     nikName: 'Stad',
//     photo: 'https://images.pexels.com/photos/6774396/pexels-photo-6774396.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
//     age: '24',
//     email:'Stad@.com'
// } 
// let res = `${person.firstName} </br> ${person.lastName} </br> ${person.nikName} </br> ${person.photo} </br> ${person.age} </br> ${person.email} `
// document.getElementById("person").innerHTML = res;


//  12
// let car = {
//     brand: 'brand - ford',
//     model: 'model - focus',
//     engine: 'engine - V8',
//     year: 'year of issue - 2014',
//     numberOfValves: 'number of valves - 32',
//     volume: 'volume - 2.5',
//     comfort: 'comfort - cruise, climate',
//     wheDiameter: 'wheel diameter - 15',
//     wheWidth: 'wheel width - 195mm',
//     loadIndex: 'load index - 91',
//     bType: 'body type - sedan',
//     trVolume: 'trunk volume - 540l',
//     numberOfSeats: 'number of seats - 5',
//     weight: 'weight - 1450kg',
//     classOfRights: 'class of rights - B'
// }

// let carInfo = `1. ${car.brand}
// </br> 2. ${car.model}
// </br> 3. ${car.engine}
// </br> 4. ${car.year}
// </br> 5. ${car.numberOfValves}
// </br> 6. ${car.volume}
// </br> 7. ${car.comfort}
// </br> 8. ${car.wheDiameter}
// </br> 9. ${car.wheWidth}
// </br> 10. ${car.loadIndex}
// </br> 11. ${car.bType}
// </br> 12. ${car.trVolume}
// </br> 13. ${car.numberOfSeats}
// </br> 14. ${car.weight}
// </br> 15. ${car.classOfRights}`

// document.getElementById("car").innerHTML = carInfo;

// let house = {
//     type: 'type - Vacation home',
//     region: 'region - Kharkiv',
//     area: 'area - 140m',
//     numberOfRooms: 'number of rooms - 4',
//     yardArea: 'yard area - 16',
//     disgraced: 'disgraced - Centralized disgraced',
//     numberOfFloors: 'number of floors - 2',
//     garage: 'garage - separate garage',
//     yeaConstruction: 'year of construction - 2008',
//     floorMaterial: 'floor material - laminate',
//     wallMaterial: 'wall material - wallblock',
//     window: 'window material - plastic',
//     waterSupply: 'water supply - central',
//     electricity: 'electricity - central',
//     neighbors: 'neighbors - peaceful'
// }

// let houseInfo = `1. ${house.type}
// </br> 2. ${house.region}
// </br> 3. ${house.area}
// </br> 4. ${house.numberOfRooms}
// </br> 5. ${house.yardArea}
// </br> 6. ${house.disgraced}
// </br> 7. ${house.numberOfFloors}
// </br> 8. ${house.garage}
// </br> 9. ${house.yeaConstruction}
// </br> 10. ${house.floorMaterial}
// </br> 11. ${house.wallMaterial}
// </br> 12. ${house.window}
// </br> 13. ${house.waterSupply}
// </br> 14. ${house.electricity}
// </br> 15. ${house.neighbors}`
// document.getElementById("house").innerHTML = houseInfo;
