/// Переменные - 1 /////
let a = 3
console.log(a)
/// Переменные - 2 /////
let a = 10
let b = 2 
let result = a + b
console.log(result)
/// Переменные - 3 /////
let a = 10
let b = 2 
let c = 5
let result = a + b + c
console.log(result)
/// Переменные - 4 /////
let a = 17
let b = 10
let c = a - b
let d = 7
let result = c + d 
console.log(result)
/// Строки - 1 /////
let text = 'Привет, мир!'
console.log(text)
/// Строки - 2 /////
let text1 = 'Привет,'
let text2 = 'Мир!'
console.log(text1 + ' ' + text2)
/// Строки - 3 /////
console.log(60 * 60) // Час
console.log(60 * 60 * 24) // День 
console.log(60 * 60 * 24 * 7) // Неделя
console.log(60 * 60 * 24 * 30) // Месяц (30дней)
/// Строки - 4 /////
let num = 1
num += 12
num -= 14
num *= 5
num /= 7
num += 1
num -= 1
console.log(num)
/// Строки - 5 /////
let hour = 'час'
let minute = 'минута'
let second = 'секунда'
let res = hour + ':' + minute + ':' + second + '!'
console.log(res)
/// Строки - 6 /////
let text = 'Я '
text += 'хочу '
text += 'знать '
text += 'JS!'
console.log(text)
/// Строки - 7 /////
let bar= 10
let foo = bar
console.log(foo)
/// Строки - 8 /////
console.log(4+5) 
console.log(7*5)  
console.log((2*2)+(3*3)) 
console.log((4+6+5)/3) 
console.log((5+1)-2*(8-2*5+3)) 
console.log(30/3*1.3)
console.log(55/5*1.2) 
console.log((100*0.4)+(100*0.84)) 
/// Строки - 9 /////
let a = 1
let b = 3 
console.log(a++ + b)
console.log(a + ++b)
console.log(++a + b++)
/// Строки - 10 /////
 let a = (5 > 2 ? 'Тебе есть 18 лет' : 'Тебе нет есть 18 лет')
 let b = ('yes' > 'no' ? 'Есть загран' : 'Нет заграна')
 let c = (28 < 43 ? 'есть 16 лет' : 'нет 16 лет')
 console.log(a)
 console.log(b)
 console.log(c)
/// Строки - 11 /////
let rand = Math.floor(Math.random() * 100)
let a = (rand % 2 ? 'Не четное' : 'Четное') + ' - ' + rand
console.log(a)
///Часть 2/////
let a = (42 < 55 ? 'Верно!' : 'Не верно!')
console.log(a)

let rand_1 = Math.floor(1 + Math.random() * (100 - 1))
let rand_2 = Math.floor(1 + Math.random() * (100 - 1))
console.log(('Максимальное число ') + Math.max(rand_1, rand_2))
console.log(('Минимальное число ') + Math.min(rand_1, rand_2))
console.log(rand_1)
console.log(rand_2)


let name = 'Дмитрий'
let family = 'Куцын'
let patronymic = 'Александрович'
console.log(family + ' ' + name[0] + '.' + ' ' + patronymic[0] + '.')

let a = 12
let b = 14
let c = 10
let d = -12
let e = 38
let f = -13
console.log(Math.max(a, b, c, d, e, f)) 
console.log(Math.min(a, b, c, d, e, f)) 

let a = 3
let b = 3
let c = 3
if (a === b && a > c && b > c ){
    console.log('Треугольник равнобедренный!')
}else if (a !== b && a !== c && c !== b){
    console.log('Треугольник разносторонний!')
}else if (a === b && b === c && c === a){
    console.log('Треугольник равносторонний!')
}

let a = 10
let b = 20
let c = 10
let d = 20
if (a === b && a === c && a === d && b === c && b === d && c === d){
    console.log('Квадрат!')
} else if (a === c && b === d){
    console.log('Прямоугольник!')
} else {
    console.log('Не существует!')
}


let month = prompt('Напишите месяц числом', '')
month = Number(month)
if ([1,2,12].includes(+month)){
    alert('Зима!')
}else if (month === 3 || month === 4 || month === 5){
    alert('Весна!')
}else if (month === 6 || month === 7 || month === 8){
    alert('Лето!')
}else {
    alert('Осень!')
}

let rand = Math.random()
console.log (rand)
let search = 5
let str = String(rand)
let res = 0
let i = 0
while(i < str.length) {
    if (search == str[i]){
        res++ 
    }
i++
}
console.log(res)


let symbols = 'asfwfsdf'
if (symbols[0] === 'a'){
    console.log('yes')
} else {
    console.log('no')
}
let a = (symbols[0] === 'a' ? 'yes' : 'no')
console.log(a)

let number = '314534'
if(number[0] === '1' || number[0] === '2' || number[0] === '3'){
    console.log('yes')
}else{
    console.log('no')
}

let test = true
if (test === true){
    console.log('Верно!')
}else if(test === false){
    console.log('Неверно!')
}
let a = (test === false ? 'Верно!' : 'Неверно!')
console.log(a)

let a = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']
let b = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
let lang = prompt('', 'ru или en')
if (lang === 'ru'){
    console.log(a)
}else if (lang === 'en'){
    console.log(b)
}else{
    console.log('Неизвестно!')
}
let c = (lang === 'ru') ? a :
  (lang === 'en') ? b : 'Неизвестно!'
  console.log(c)

let minute = prompt('Напишите до 59', '')
if (minute <= 15){
    console.log('Первая четверть часа')
}else if (minute <= 30){
    console.log('Вторая четверть часа')
}else if (minute <= 45){
    console.log('Третья четверть часа')
}else if (minute <= 59){
    console.log('Четвертая четверть часа')
}else{
    console.log('Неизвестное число!')
}
let a = (minute <= 15) ? console.log('Первая четверть часа') : 
(minute <= 30) ? console.log('Вторая четверть часа') :
(minute <= 45) ? console.log('Третья четверть часа') :
(minute <= 59) ? console.log('Четвертая четверть часа') :
console.log('Неизвестное число!')




